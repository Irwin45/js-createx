import Swiper, { Navigation, Pagination, Thumbs } from "swiper";

Swiper.use([Navigation, Pagination, Thumbs]);

const gap = parseInt(
  getComputedStyle(document.documentElement).getPropertyValue("--grid-gap")
);

const portSlider = document.querySelector(".portfolio-section__items");

const portfolioSlider = new Swiper(portSlider, {
  slidesPerView: 3,
  spaceBetween: gap,
  // on: {
  //   init: function () {
  //     console.log("init");

  //     const activeSlide = portSlider.querySelector(".swiper-slide-active");

  //     const nextActiveSlide = activeSlide.nextElementSibling;

  //     const nextNextActiveSlide = nextActiveSlide.nextElementSibling;

  //     activeSlide.classList.add("slider-visible");
  //     nextActiveSlide.classList.add("slider-visible");
  //     nextNextActiveSlide.classList.add("slider-visible");
  //   },
  // },
  navigation: {
    nextEl: ".portfolio-section__next",
    prevEl: ".portfolio-section__prev",
  },
});

// portfolioSlider.on("slideChange", function () {
//   console.log("change");

//   const activeSlide = portSlider.querySelector(".swiper-slide-next");

//   const nextActiveSlide = activeSlide.nextElementSibling;

//   const nextNextActiveSlide = nextActiveSlide.nextElementSibling;

//   document
//     .querySelectorAll(".portfolio-section__items .swiper-slide")
//     .forEach((el) => {
//       el.classList.remove("slider-visible");
//     });

//   activeSlide.classList.add("slider-visible");
//   nextActiveSlide.classList.add("slider-visible");
//   nextNextActiveSlide.classList.add("slider-visible");
// });

// document
//   .querySelector(".portfolio-section__prev")
//   .addEventListener("click", () => {
//     const activeSlide = portSlider.querySelector(".swiper-slide-next");

//     if (activeSlide.previousElementSibling) {
//       const nextActiveSlide = activeSlide.previousElementSibling;
//       nextActiveSlide.classList.add("slider-visible");
//     }

//     //
//     // const nextNextActiveSlide = nextActiveSlide.previousElementSibling;

//     // document
//     //   .querySelectorAll(".portfolio-section__items .swiper-slide")
//     //   .forEach((el) => {
//     //     el.classList.remove("slider-visible");
//     //   });

//     // activeSlide.classList.add("slider-visible");
//     // nextActiveSlide.classList.add("slider-visible");
//     // nextNextActiveSlide.classList.add("slider-visible");
//   });

// document
//   .querySelector(".portfolio-section__next")
//   .addEventListener("click", () => {
//     const activeSlide = portSlider.querySelector(".swiper-slide-active");

//     const nextActiveSlide = activeSlide.nextElementSibling;

//     const nextNextActiveSlide = nextActiveSlide.nextElementSibling;

//     document
//       .querySelectorAll(".portfolio-section__items .swiper-slide")
//       .forEach((el) => {
//         el.classList.remove("slider-visible");
//       });

//     activeSlide.classList.add("slider-visible");
//     nextActiveSlide.classList.add("slider-visible");
//     nextNextActiveSlide.classList.add("slider-visible");
//   });

const testimonialSlider = new Swiper(".testimonials__items", {
  slidesPerView: 1,
  loop: true,
  spaceBetween: gap,
  navigation: {
    nextEl: ".testimonials__next",
    prevEl: ".testimonials__prev",
  },
});

const relatedSlider = new Swiper(".related-projects__items", {
  slidesPerView: 3,
  spaceBetween: gap,
  // on: {
  //   init: function () {
  //     console.log("init");

  //     const activeSlide = portSlider.querySelector(".swiper-slide-active");

  //     const nextActiveSlide = activeSlide.nextElementSibling;

  //     const nextNextActiveSlide = nextActiveSlide.nextElementSibling;

  //     activeSlide.classList.add("slider-visible");
  //     nextActiveSlide.classList.add("slider-visible");
  //     nextNextActiveSlide.classList.add("slider-visible");
  //   },
  // },
  navigation: {
    nextEl: ".related-projects__next",
    prevEl: ".related-projects__prev",
  },
});

const workImages = document.querySelector(".work-images-slider");

if (workImages) {
  const workSlider = new Swiper(".work-images-nav", {
    spaceBetween: 20,
    slidesPerView: 10,
    freeMode: true,
    watchSlidesProgress: true,
  });
  const workSliderNav = new Swiper(workImages, {
    spaceBetween: 20,
    slidesPerView: 1,
    navigation: {
      nextEl: ".work-images__next",
      prevEl: ".work-images__prev",
    },
    thumbs: {
      swiper: workSlider,
    },
  });
}

const historySlider = document.querySelector(".history-slider");

if (historySlider) {
  const workSlider = new Swiper(historySlider, {
    spaceBetween: 20,
    slidesPerView: 1,
    navigation: {
      nextEl: ".history__next",
      prevEl: ".history__prev",
    },
  });

  workSlider.on("slideChange", function () {
    console.log(workSlider.realIndex);

    historyBtns.forEach((el) => {
      el.classList.remove("history-nav__btn--active");
    });

    document
      .querySelector(`.history-nav__btn[data-index="${workSlider.realIndex}"]`)
      .classList.add("history-nav__btn--active");
  });

  const historyBtns = document.querySelectorAll(".history-nav__btn");

  historyBtns.forEach((el, idx) => {
    el.setAttribute("data-index", idx);

    el.addEventListener("click", (e) => {
      const index = e.currentTarget.dataset.index;

      historyBtns.forEach((el) => {
        el.classList.remove("history-nav__btn--active");
      });

      e.currentTarget.classList.add("history-nav__btn--active");

      workSlider.slideTo(index);
    });
  });
}

const heroSlider = new Swiper(".hero-slider", {
  slidesPerView: 1,
  navigation: {
    nextEl: ".hero__next",
    prevEl: ".hero__prev",
  },

  pagination: {
    el: ".hero__pag",
    type: "bullets",
    clickable: true,
  },

  on: {
    init() {
      const paginationBullets = document.querySelectorAll(
        ".hero__pag .swiper-pagination-bullets"
      );

      paginationBullets.forEach((el) => {
        el.innerHTML = `<span class="hero__bar"></span>`;
      });
    },
  },
});
